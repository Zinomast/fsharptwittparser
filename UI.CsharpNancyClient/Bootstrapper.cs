﻿using Nancy.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;

namespace UI.CsharpNancyClient
{
	public class Bootstrapper : DefaultNancyBootstrapper
	{
		protected override void ConfigureConventions(NancyConventions nancyConventions)
		{
			nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("Content", @"Content"));
			nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("Scripts", @"Scripts"));
			base.ConfigureConventions(nancyConventions);
		}
	}
}