﻿(function () {
	"use strict";
	angular
        .module("controllerModule")
        .controller("mainController", mainController);
	mainController.$inject = ["$http"];
	function mainController($http) {
		/* jshint validthis:true */
		var vm = this;
		vm.title = "mainController";
		vm.is = {
			DataAvailable: false,
			ParseError: false,
			ServerError: false
		};
		vm.isDataAvailable = false;
		vm.inputStr = "";
		vm.err = {
			parse_error: "An error occured while parsing your request. Maybe that wasn`t status?",
			server_error: "Something with our server! I`m sorry. Please try again later."
		}
		vm.output = {
			imgs: []
		}
		//activate();
		//function activate() {

		//}
		vm.postInput = function () {
			resetAllErrors();
			resetBoolModels();
			var serverUrl = "http://localhost:48213/twitter/v1/parse";
			$http({
				method: "POST",
				url: serverUrl,
				data: { Status: vm.inputStr }
			}).then(function (response) {
				populate(response.data);
			}, function (response) {
				processErrors(response);
			});
		}
		function resetAllErrors() {
			vm.is.ParseError = false;
			vm.is.ServerError = false;
		}
		function resetBoolModels() {
			vm.is.DataAvailable = false;
			vm.output.imgs = "";
		}
		function populate(data) {
			var arr = [];
			for (var i = 1; i <= data.length; i++) {
				arr.push({ id: i, src: data[i - 1] });
			}
			vm.output.imgs = arr;
			vm.is.DataAvailable = true;
		}
		function processErrors(response) {
			switch (response.status) {
				case 400:
					vm.is.ParseError = true;
					$("#parsing-error").show();
					break;
				case 401:
					break;
				case 404:
					break;
				case 500:
					vm.is.ServerError = true;
					$("#server-error").show();
				default:
			}
		}
	}
})();