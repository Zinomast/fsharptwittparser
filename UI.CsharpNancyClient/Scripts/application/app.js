﻿(function () {
	"use strict";
	angular.module("app", [
        // Angular modules 
        // Custom modules 
		"controllerModule",
		"directiveModule"
        // 3rd Party Modules
	]);
	angular.module("controllerModule", [
        // Angular modules 
        // Custom modules 
        // 3rd Party Modules
	]);
	angular.module("directiveModule", [
        // Angular modules 
        // Custom modules 
        // 3rd Party Modules
	]);
	angular
		.module("directiveModule")
		.directive('ngEnter', function () {
			return function (scope, element, attrs) {
				element.bind("keydown keypress", function (event) {
					if (event.which === 13) {
						scope.$apply(function () {
							scope.$eval(attrs.ngEnter, { 'event': event });
						});
						event.preventDefault();
					}
				});
			};
		});
})();