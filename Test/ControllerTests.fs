﻿namespace Test

open Xunit
open API.WebAPI
open System.Web.Http
open ViewModels
open System.Net
open FSharp.Core
open ValidationService
open DownloadService
open ParserService

module ControllerTests = 
    open System.Net.Http
    open System.Web.Http.Results
    open System.Collections.Generic
    
    let twittController = new TwitterController()
    
    [<Fact>]
    let CheckIfResponseIsNotInternaServerError() = 
        let request = { Status = "https://twitter.com/rarlillrap/status/735453483634024448" }
        let links = twittController.ParseAsync(request)
        links.Wait()
        let result = links.Result :?> NegotiatedContentResult<IEnumerable<string>>
        Assert.False(result.StatusCode = HttpStatusCode.InternalServerError)
    [<Fact>]
    let CheckIfResponseIsOK() =
        let request = { Status = "https://twitter.com/rarlillrap/status/735453483634024448" }
        let links = twittController.ParseAsync(request)
        links.Wait()
        let result = links.Result :?> NegotiatedContentResult<IEnumerable<string>>
        Assert.True(result.StatusCode = HttpStatusCode.OK)
    [<Fact>]
    let CheckIfResponseIsBadRequest() =
        let request = { Status = "bad_request" }
        let links = twittController.ParseAsync(request)
        links.Wait()
        let result = links.Result :?> NegotiatedContentResult<string>
        Assert.True(result.StatusCode = HttpStatusCode.BadRequest)
