﻿namespace API.WebAPI

open System.Net
open System.Net.Http
open System.Web.Http
open System.Web.Http.Results
open System.Web.Http.Description
open System.Threading.Tasks
open System.Collections.Generic
open ValidationService
open DownloadService
open ParserService
open FSharp.Data
open FSharp.Collections

[<RoutePrefix("twitter/v1")>]
type TwitterController() = 
    class
        inherit ApiController()
        let mutable ActionModelState = base.ModelState
        let mutable ActionRequest = base.Request

        member private __.Result input = 
            match input with
            | Failure error -> base.Content(HttpStatusCode.BadRequest, error) :> IHttpActionResult
            | Success result -> base.Content(HttpStatusCode.OK, result) :> IHttpActionResult
        
        [<Route("parse"); HttpPost>]
        member __.ParseAsync([<FromBody>] request : RequestRecord) : Task<IHttpActionResult> =
            async { 
                let! processResult = 
                        async 
                            { 
                                let! url = TryValidateAsyncly <| request.Status
                                let! html = TryDownloadAsyncly <| url
                                let! links = TryParseAsyncly <| html
                                return links 
                            }
                return __.Result processResult 
            } |> Async.StartAsTask
    end
