﻿module WebApiConfig

open System.Web.Http

type RouteParameterRecord = {
    id:RouteParameter
}

let RegisterRoutes (config: HttpConfiguration) =
    config.MapHttpAttributeRoutes()
    config.Routes.MapHttpRoute(
        "DefaultApi",
        "api/{controller}/{action}/{id}",
        { id = RouteParameter.Optional } 
        |> ignore
    ) |> ignore
    config.Formatters.XmlFormatter.UseXmlSerializer <- true
    config.Formatters.JsonFormatter.SerializerSettings.ContractResolver <- Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()

let RegisterWebApi (config: HttpConfiguration) =
    config
    |> RegisterRoutes

