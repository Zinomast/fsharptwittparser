﻿namespace API.WebAPI

open System
open System.IO
open FSharp.Data
open System.Net
open System.Text.RegularExpressions
open FSharp.Collections

module Validators = 
    let twittRegex = @"^http(s)?:\/\/twitter\.com\/(?:#!\/)?(\w+)\/status(es)?\/(\d+)$"
    
    let CheckIfEmpty input = 
        match String.IsNullOrEmpty input with
        | true -> Failure "empty string"
        | false -> Success input
    
    let CheckIfWhiteSpace input = 
        match String.IsNullOrWhiteSpace input with
        | true -> Failure "empty or whitespace"
        | false -> Success input
    
    let CheckIfNotMatched regex input = 
        let result = Regex.IsMatch(input, regex)
        match result with
        | false -> Failure "input is not matching"
        | true -> Success input
    
    let TwittRegexMatch = CheckIfNotMatched twittRegex
    let Validator = CheckIfEmpty >=> CheckIfWhiteSpace >=> TwittRegexMatch

module ValidationService = 
    open Validators
    
    let TryValidate = Try Validator
    let TryValidateAsyncly url = AsAsync TryValidate url

module Downloaders = 
    let Download url = 
        let uri = new Uri(url)
        let webClient = new WebClient()
        let result = webClient.DownloadString uri
        result

module DownloadService = 
    open Downloaders
    
    let TryDownload = Try(Manage Download)
    let TryDownloadAsyncly url = AsAsync TryDownload url

module Parsers = 
    let Filter attr (node : HtmlNode) = 
        node.DescendantsAndSelf [ "img" ] |> Seq.map (fun img -> img.AttributeValue attr)
    
    let Parse resource = 
        let document = HtmlDocument.Parse resource
        
        let links = 
            document.Descendants [ "div" ]
            |> Seq.map (fun d -> d.CssSelect ".AdaptiveMedia" |> List.toSeq)
            |> Seq.concat
            |> Seq.map (fun d -> d |> Filter "src")
            |> Seq.concat
            |> Seq.distinct
        links

module ParserService = 
    open Parsers
    
    let TryParse = Try(Manage Parse)
    let TryParseAsyncly url = AsAsync TryParse url
