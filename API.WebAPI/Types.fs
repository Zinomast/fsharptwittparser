﻿namespace API.WebAPI

open System.ComponentModel.DataAnnotations
open FSharp.Collections
[<AutoOpen>]
module ViewModels = 
    type RequestRecord = 
        { [<Required; RegularExpression(@"^http(s)?:\/\/twitter\.com\/(?:#!\/)?(\w+)\/status(es)?\/(\d+)$", 
                                        ErrorMessage = "")>]
          Status : string }
    
    type RequestViewModel() = 
        class
            [<Required; RegularExpression(@"^http(s)?:\/\/twitter\.com\/(?:#!\/)?(\w+)\/status(es)?\/(\d+)$", 
                                          ErrorMessage = "Invalid status url")>]
            member val Status = ""
        end

[<AutoOpen>]
module Helpers = 
    type Result<'TSuccess, 'TFailure> = 
        | Success of 'TSuccess
        | Failure of 'TFailure
    
    let succeed x = Success x
    let fail x = Failure x
    let AsAsync func value = async { return func value }
    
    let Try func value = 
        try 
            func value
        with exc -> Failure exc.Message
    
    let Bind switch = 
        function 
        | Success value -> switch value
        | Failure error -> Failure error
    
    let Manage func resource = 
        match resource with
        | Failure error -> Failure error
        | Success value -> func value |> Success
    
    let (==>>) value switch = Bind switch value
    let (>=>) switch' switch'' = switch' >> Bind switch''

    let (||>>) a b =
        async.Bind (a, b >> async.Return)