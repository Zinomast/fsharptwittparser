namespace API.WebAPI
open Owin
open Microsoft.Owin
open System
open System.Net.Http
open System.Web.Http
open System.Web.Http.Owin
open WebApiConfig
open Microsoft.Owin.Cors

[<Sealed>]
type Startup() = 
    member __.Configuration(builder : IAppBuilder) = 
        let config = new HttpConfiguration()
        config
        |> RegisterWebApi
        |> ignore
        builder.UseCors(CorsOptions.AllowAll) |> ignore
        builder.UseWebApi(config) |> ignore
        builder.UseWelcomePage()
        |> ignore
