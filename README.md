**TwittParser(F#)** - a web-service that is used to parse twitter statuses for media content. My TwittParser project rewritten using F#.

**Technologies used:** 

*    F#, 
*    asp.net web api + owin + CORS, 
*    autofac,
*    F# Data,
*    Bootstrap, 
*    Angularjs. 